   Basic Configuration Files
===============================

   Tools
-----------

- [vim](https://www.vim.org/) - text editor ([vimrc](files/vimrc), default location: `~/.vimrc`)
- [tmux](https://github.com/tmux/tmux/wiki) - terminal muliplexer ([tmux.conf](files/tmux.conf), default location: `~/.tmux.conf`)
- [terminator](https://gnometerminator.blogspot.com/p/introduction.html) - terminal emulator ([terminator](files/terminator), default location: `~/.config/terminator/config`)
- [taskwarrior](https://taskwarrior.org/) - TODO list manager ([task](files/taskrc), default location: `~/.taskrc`)



